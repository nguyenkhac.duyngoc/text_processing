import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
import matplotlib.image as mpimg

def canny_edge_detection(img, grad_x, grad_y):
    return cv2.Canny(img, grad_x, grad_y)

def median_filter(img, ksize):
    """[summary]

    Arguments:
        img {[img]} -- [input image]
        ksize {[type]} -- [aperture linear size; it must be odd and greater than 1, for example: 3, 5, 7 ...]

    Returns:
        [img] -- [output image]
    """
    return cv2.medianBlur(img, ksize)

def binary_dilation(img, ksize, iterations):
    kernel = np.ones((ksize, ksize), np.uint8)
    return cv2.dilate(img, kernel, iterations=iterations)

def find_components(img, max_components=16):
    count = 100
    n =1
    img_dilation = img
    while count > max_components:
        print(n)
        img_dilation = binary_dilation(img_dilation, 7, 1)
        # plt.imshow(img_dilation)
        # plt.show()
        # ret, thresh=cv2.threshold(img_dilation,250,255,1)
        
        # plt.imshow(img_dilation)
        # plt.show()
        contours, hierarchy = cv2.findContours(img_dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        count = len(contours)
        
        print("Count:", count)
        n += 1
    return contours

def find_optimal_components_subset(contours, edges):
    """Find a crop which strikes a good balance of coverage/compactness.
    Returns an (x1, y1, x2, y2) tuple.
    """
    c_info = props_for_contours(contours, edges)
    print("Contour_info: ", c_info)
    c_info.sort(key=lambda x: -x['sum'])
    total = np.sum(edges) / 255
    area = edges.shape[0] * edges.shape[1]
    print("W:", edges.shape[0])
    print("H:", edges.shape[1])

    c = c_info[0]
    del c_info[0]
    this_crop = c['x1'], c['y1'], c['x2'], c['y2']
    print(this_crop)
    crop = this_crop
    covered_sum = c['sum']

    while covered_sum < total:
        changed = False
        recall = 1.0 * covered_sum / total
        prec = 1 - 1.0 * crop_area(crop) / area
        f1 = 2 * (prec * recall / (prec + recall))
        #print '----'
        for i, c in enumerate(c_info):
            this_crop = c['x1'], c['y1'], c['x2'], c['y2']
            print("This_crop: ", this_crop)
            new_crop = union_crops(crop, this_crop)
            print("New_crop: ", new_crop)
            new_sum = covered_sum + c['sum']
            new_recall = 1.0 * new_sum / total
            new_prec = 1 - 1.0 * crop_area(new_crop) / area
            new_f1 = 2 * new_prec * new_recall / (new_prec + new_recall)

            # Add this crop if it improves f1 score,
            # _or_ it adds 25% of the remaining pixels for <15% crop expansion.
            # ^^^ very ad-hoc! make this smoother
            remaining_frac = c['sum'] / (total - covered_sum)
            new_area_frac = 1.0 * crop_area(new_crop) / crop_area(crop) - 1
            if new_f1 > f1 or (
                    remaining_frac > 0.25 and new_area_frac < 0.15):
                print('%d %s -> %s / %s (%s), %s -> %s / %s (%s), %s -> %s' % (
                        i, covered_sum, new_sum, total, remaining_frac,
                        crop_area(crop), crop_area(new_crop), area, new_area_frac,
                        f1, new_f1))
                crop = new_crop
                covered_sum = new_sum
                del c_info[i]
                changed = True
                break

        if not changed:
            break

    return crop

def pad_crop(crop, contours, edges, border_contour=None, pad_px=15):
    """Slightly expand the crop to get full contours.
    This will expand to include any contours it currently intersects, but will
    not expand past a border.
    """
    bx1, by1, bx2, by2 = 0, 0, edges.shape[0], edges.shape[1]
    if border_contour is not None and len(border_contour) > 0:
        c = props_for_contours([border_contour], edges)[0]
        bx1, by1, bx2, by2 = c['x1'] + 5, c['y1'] + 5, c['x2'] - 5, c['y2'] - 5

    def crop_in_border(crop):
        x1, y1, x2, y2 = crop
        x1 = max(x1 - pad_px, bx1)
        y1 = max(y1 - pad_px, by1)
        x2 = min(x2 + pad_px, bx2)
        y2 = min(y2 + pad_px, by2)
        return crop

    crop = crop_in_border(crop)

    c_info = props_for_contours(contours, edges)
    changed = False
    for c in c_info:
        this_crop = c['x1'], c['y1'], c['x2'], c['y2']
        this_area = crop_area(this_crop)
        int_area = crop_area(intersect_crops(crop, this_crop))
        new_crop = crop_in_border(union_crops(crop, this_crop))
        if 0 < int_area < this_area and crop != new_crop:
            print('%s -> %s' % (str(crop), str(new_crop)))
            changed = True
            crop = new_crop

    if changed:
        return pad_crop(crop, contours, edges, border_contour, pad_px)
    else:
        return crop

def props_for_contours(contours, ary):
    """Calculate bounding box & the number of set pixels for each contour."""
    c_info = []
    for c in contours:
        x,y,w,h = cv2.boundingRect(c)
        c_im = np.zeros(ary.shape)
        cv2.drawContours(c_im, [c], 0, 255, -1)
        c_info.append({
            'x1': x,
            'y1': y,
            'x2': x + w - 1,
            'y2': y + h - 1,
            'sum': np.sum(ary * (c_im > 0))/255
        })

    return c_info

def crop_area(crop):
    x1, y1, x2, y2 = crop
    return max(0, x2 - x1) * max(0, y2 - y1)

def union_crops(crop1, crop2):
    """Union two (x1, y1, x2, y2) rects."""
    x11, y11, x21, y21 = crop1
    x12, y12, x22, y22 = crop2
    return min(x11, x12), min(y11, y12), max(x21, x22), max(y21, y22)

def intersect_crops(crop1, crop2):
    x11, y11, x21, y21 = crop1
    x12, y12, x22, y22 = crop2
    return max(x11, x12), max(y11, y12), min(x21, x22), min(y21, y22)

def add_gaussian_noise(img):
    gauss = np.random.normal(0, 1, img.size)
    gauss = gauss.reshape(img.shape[0], img.shape[1]).astype('uint8')
    img_gauss = cv2.add(img,gauss)
    return img_gauss

if __name__ == "__main__":
    INPUT = "/home/duyngoc/project/text_processing/Data/bill.crop.png"
    out_path = INPUT.replace('.jpg', '.crop.png')
    img = cv2.imread(INPUT)
    # plt.title("Input")
    # plt.imshow(img)
    # plt.show()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # plt.title("RGB to Gray")
    # plt.imshow(img)
    # plt.show()
    thresh = cv2.threshold(gray, 175, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    result = thresh.copy()

    # Remove horizontal lines
    horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40,1))
    remove_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
    cnts = cv2.findContours(remove_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(result, [c], -1, (0, 0, 0), 5)

    # Remove vertical lines
    vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,40))
    remove_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
    cnts = cv2.findContours(remove_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(result, [c], -1, (0, 0, 0), 5)
    

    # plt.title("Thresh")
    # plt.imshow(thresh)
    # plt.show()
    # plt.title("Result")
    # plt.imshow(result)
    # plt.show()

    canny_edge_img = canny_edge_detection(result, 100, 200)
    # plt.title("Canny")
    # plt.imshow(canny_edge_img)
    # plt.show()

    # canny_edge_img = add_gaussian_noise(canny_edge_img)
    # plt.imshow(canny_edge_img)
    # plt.show()

    # median_filter_img = cv2.GaussianBlur(canny_edge_img,(5,5),0)
    # plt.imshow(median_filter_img)
    # plt.show()

    # median_filter_img = median_filter(canny_edge_img, 5)
    # plt.title("Median filter")
    # plt.imshow(median_filter_img)
    # plt.show()

    contours = find_components(canny_edge_img)
    print(contours)
    if len(contours) == 0:
        print('%s -> (no text!)' % INPUT)
    else:
        print('Contours length:', len(contours))

    for c in contours:
        x,y,w,h =cv2.boundingRect(c)
        print(x,y,w,h)
        # c_im = np.zeros(canny_edge_img.shape)
        # cv2.drawContours(c_im, [c], 0, 255, 3)
        cv2.rectangle(img, (x, y), (x+w -1, y+h -1), (0, 0, 255), 15)
    
    plt.imshow(img)
    plt.show()


    # crop = find_optimal_components_subset(contours, median_filter_img)
    # print(crop)
    # crop = pad_crop(crop, contours, median_filter_img)
    # crop = [int(x) for x in crop]  # upscale to the original image size.
    # orig_im = Image.open(INPUT)



    # text_im = orig_im.crop(crop)
    # text_im.save(out_path)
    # print('%s -> %s' % (INPUT, out_path))

    # print(median_filter_img)
    # img_dilation = binary_dilation(median_filter_img, 5, 1)
    # imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # ret, thresh = cv2.threshold(img, 127, 255, 0)
    # contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # cv2.drawContours(img, contours, -1, (0,0,0), 3)
    # cv2.imwrite('img_dilation.jpg', img_dilation)
